<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGraduationUndangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graduation_undangan', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('undangan');
            $table->enum('kategori',['Wisudawan','Orang Tua','Undangan Khusus','Direksi Jaya','Pimpinan UPJ','Dosen & Tendik','Orator/Narasumber','Undangan Tambahan']);
            $table->unsignedBigInteger('tahun_id');
            $table->timestamps();

            $table->foreign('tahun_id')->on('graduation_tahun')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('graduation_undangan', function (Blueprint $table) {
            $table->dropForeign(['tahun_id']);
        });
        Schema::dropIfExists('graduation_undangan');
    }
}
