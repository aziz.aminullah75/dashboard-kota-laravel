<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGraduationSaranaPrasarana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graduation_sarana_prasarana', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->string('kategori');
            $table->unsignedBigInteger('tahun_id');
            $table->timestamps();

            $table->foreign('tahun_id')->on('graduation_tahun')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graduation_sarana_prasarana');

        Schema::table('graduation_sarana_prasarana', function (Blueprint $table) {
            $table->dropForeign(['tahun_id']);
        });
    }
}
