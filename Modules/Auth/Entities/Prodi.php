<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Urban;

class Prodi extends Model
{
    protected $fillable = ['nama_prodi', 'singkatan_prodi', 'urban_id', 'logo'];


    protected $table = 'prodi';

    public function urban()
    {
        return $this->belongsTo(Urban::class, 'urban_id');
    }
}
