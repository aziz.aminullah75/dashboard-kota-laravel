<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class Urban extends Model
{
    protected $fillable = ['nama_urban'];

    protected $table = 'urban';
}
