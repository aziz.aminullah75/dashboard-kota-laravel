<?php

namespace Modules\Auth\Http\Controllers;

//use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\Urban;

class UrbanController extends Controller
{
    private $urbanModel;
    public function __construct()
    {
        $this->urbanModel = new Urban();
    }


    public function index()
    {
        $getAllUrban = $this->urbanModel->get(); // select * from Fakultass;
        return response()->json($getAllUrban);
    }

    public function store(Request $request)
    {
        $createNewUrban = $this->urbanModel->create([
            'nama_urban' => $request->nama_urban,
        ]);
        return response()->json($createNewUrban);
    }

    public function show($id)
    {
        $findUrban = $this->urbanModel->find($id);
        return response()->json($findUrban);
    }

    public function update($id, Request $request)
    {
        $findUrban = $this->urbanModel->find($id);
        $findUrban->update([
            'nama_urban' => $request->nama_urban,
        ]);
        return response()->json($findUrban);
    }

    public function destroy($id)
    {
        $findUrban = $this->urbanModel->find($id);
        $findUrban->delete();
        return response()->json($findUrban);
    }
}
