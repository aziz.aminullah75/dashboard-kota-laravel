<?php

namespace Modules\Auth\Http\Controllers;

use Modules\Auth\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class UsersController extends Controller
{
    private $usersModel;
    public function __construct()
    {
        $this->usersModel = new User();
    }

    public function index(Request $request)
    {
        // $userModel = $this->usersModel->with('prodi');
        $getAllUsers = $this->usersModel->with('prodi')->get();
        return response()->json($getAllUsers);
    }

    public function store(Request $request)
    {
        // $userModel = $this->usersModel;
        // dd($request->level);
        // $createNewUsers = $this->usersModel->get();
        $createNewUsers = $this->usersModel->create([
            'prodi_id' => $request->prodi_id,
            'level' => $request->level ?? 'admin',
            'username' => $request->username,
            'name' => $request->name,
            'password' => bcrypt($request->password)
        ]);
        return response()->json($createNewUsers);
    }

    public function show($id)
    {
        $findUsers = $this->usersModel->find($id);
        $findUsers = $this->usersModel->with('prodi')->find($id);
        return response()->json($findUsers);
    }

    public function update($id, Request $request)
    {
        $findUsers = $this->usersModel->find($id);
        $findUsers->update([
            'prodi_id' => $request->prodi_id,
            'level' => $request->level,
            'username' => $request->username,
            'name' => $request->name,
            'password' => bcrypt($request->password)
        ]);
        return response()->json($findUsers);
    }

    public function destroy($id)
    {
        $findUsers = $this->usersModel->find($id);
        $findUsers->delete();
        return response()->json($findUsers);
    }
}
