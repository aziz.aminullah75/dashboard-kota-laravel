<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('periode_id');
            $table->unsignedBigInteger('mata_kuliah_id');
            $table->unsignedBigInteger('jadwal_id');
            $table->unsignedBigInteger('dosen_id');
            $table->string('ruangan');
            $table->string('kuota_dibuka');
            $table->string('kuota_terisi')->nullable();
            $table->string('cadangan')->nullable();
            $table->timestamps();

            $table->foreign('periode_id')->on('periode')->references('id')->onDelete('cascade');
            $table->foreign('mata_kuliah_id')->on('mata_kuliah')->references('id')->onDelete('cascade');
            $table->foreign('jadwal_id')->on('jadwal')->references('id')->onDelete('cascade');
            $table->foreign('dosen_id')->on('dosen')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan', function (Blueprint $table) {
            $table->dropForeign(['periode_id']);
        });

        Schema::table('laporan', function (Blueprint $table) {
            $table->dropForeign(['mata_kuliah_id']);
        });

        Schema::table('laporan', function (Blueprint $table) {
            $table->dropForeign(['jadwal_id']);
        });

        Schema::table('dosen', function (Blueprint $table) {
            $table->dropForeign(['dosen_id']);
        });

        Schema::dropIfExists('laporan');
    }
}
