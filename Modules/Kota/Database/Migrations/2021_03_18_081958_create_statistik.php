<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatistik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistik', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('laporan_id');
            $table->string('total_kuota_dibuka');
            $table->string('total_kuota_terisi');
            $table->timestamps();

            $table->foreign('laporan_id')->on('laporan')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistik', function (Blueprint $table) {
            $table->dropForeign(['laporan_id']);
        });

        Schema::dropIfExists('statistik');
    }
}
