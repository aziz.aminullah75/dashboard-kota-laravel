<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_priority', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('periode_id');
            $table->unsignedBigInteger('mata_kuliah_id');
            $table->string('peserta');
            $table->timestamps();

            $table->foreign('users_id')->on('users')->references('id')->onDelete('cascade');
            $table->foreign('periode_id')->on('periode')->references('id')->onDelete('cascade');
            $table->foreign('mata_kuliah_id')->on('mata_kuliah')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_priority', function (Blueprint $table) {
            $table->dropForeign(['users_id']);
        });

        Schema::table('booking_priority', function (Blueprint $table) {
            $table->dropForeign(['periode_id']);
        });

        Schema::table('booking_priority', function (Blueprint $table) {
            $table->dropForeign(['mata_kuliah_id']);
        });

        Schema::dropIfExists('booking_priority');
    }
}
