<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Kota\Entities\Periode;

class PeriodeController extends Controller
{
    private $periodeModel;
    public function __construct()
    {
        $this->periodeModel = new Periode();
    }

    public function index()
    {
        $getAllPeriode = $this->periodeModel->get(); // select * from Periode;
        return response()->json($getAllPeriode);
    }

    public function store(Request $request)
    {
        $createNewPeriode = $this->periodeModel->create([
            'tahun' => $request->tahun,
            'semester' => $request->semester,
        ]);
        return response()->json($createNewPeriode);
    }

    public function show($id)
    {
        $findPeriode = $this->periodeModel->find($id);
        return response()->json($findPeriode);
    }

    public function update($id, Request $request)
    {
        $findPeriode = $this->periodeModel->find($id);
        $findPeriode->update([
            'tahun' => $request->tahun,
            'semester' => $request->semester,
        ]);
        return response()->json($findPeriode);
    }

    public function destroy($id)
    {
        $findPeriode = $this->periodeModel->find($id);
        $findPeriode->delete();
        return response()->json($findPeriode);
    }
}
