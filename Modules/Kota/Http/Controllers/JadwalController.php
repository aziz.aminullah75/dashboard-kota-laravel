<?php

namespace Modules\Kota\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Kota\Entities\Jadwal;

class JadwalController extends Controller
{
    private $jadwalModel;
    public function __construct()
    {
        $this->jadwalModel = new Jadwal();
    }

    public function index()
    {
        $getAllJadwal = $this->jadwalModel->get(); // select * from Jadwal;
        return response()->json($getAllJadwal);
    }

    public function store(Request $request)
    {
        $createNewJadwal = $this->jadwalModel->create([
            'hari' => $request->hari,
            'jam' => $request->jam,
        ]);
        return response()->json($createNewJadwal);
    }

    public function show($id)
    {
        $findJadwal = $this->jadwalModel->find($id);
        return response()->json($findJadwal);
    }

    public function update($id, Request $request)
    {
        $findJadwal = $this->jadwalModel->find($id);
        $findJadwal->update([
            'hari' => $request->hari,
            'jam' => $request->jam,
        ]);
        return response()->json($findJadwal);
    }

    public function destroy($id)
    {
        $findJadwal = $this->jadwalModel->find($id);
        $findJadwal->delete();
        return response()->json($findJadwal);
    }
}