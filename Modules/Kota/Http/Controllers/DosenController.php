<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Kota\Entities\Dosen;

class DosenController extends Controller
{
    private $dosenModel;
    public function __construct()
    {
        $this->dosenModel = new Dosen();
    }

    // public function index()
    // {
    //     $getAllDosen = $this->dosenModel->with('prodi')->get(); // select * from MataKuliah;
    //     return response()->json($getAllDosen);
    // }

    public function index(Request $request)
    {
        $getAllDosen = $this->dosenModel->with('prodi'); // select * from MataKuliah;

        $prodi = $request->prodi_id;

        if($request->filled('prodi_id')){
            $getAllDosen = $getAllDosen->whereHas('prodi',function($query) use ($prodi){
                return $query->where('id',$prodi);
            });
        }

        $getAllDosen = $getAllDosen->get(); // select * from MataKuliah;
        return response()->json($getAllDosen);
    }

    public function store(Request $request)
    {
        $createNewDosen = $this->dosenModel->create([
            'prodi_id' => $request->prodi_id,
            'nama' => $request->nama,
            'nik' => $request->nik,
            'jk' => $request->jk,
            'email' => $request->email,
        ]);
        return response()->json($createNewDosen);
    }

    public function show($id)
    {
        $findDosen = $this->dosenModel->with('prodi')->find($id);
        return response()->json($findDosen);
    }

    public function update($id, Request $request)
    {
        $findDosen = $this->dosenModel->find($id);
        $findDosen->update([
            'prodi_id' => $request->prodi_id,
            'nama' => $request->nama,
            'nik' => $request->nik,
            'jk' => $request->jk,
            'email' => $request->email,
        ]);
        return response()->json($findDosen);
    }

    public function destroy($id)
    {
        $findDosen = $this->dosenModel->find($id);
        $findDosen->delete();
        return response()->json($findDosen);
    }

    public function kaprodiDosen (Request $request)
    {

        $dosenProdi = auth()->user()->prodi_id;

        $kaprodiDosen = Dosen::with(['prodi'])
        // ->join('dosen','dosen.id','=','id')
        ->join('prodi','prodi.id','=','dosen.prodi_id')
        ->where('prodi_id',$dosenProdi)
        ->selectRaw('dosen.id , dosen.nama')
        ->groupBy('id','nama')
        ->get();

        return response()->json(['data' => $kaprodiDosen]);

    }
}
