<?php

namespace Modules\Kota\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Storage;
use Modules\Kota\Entities\MataKuliah;

class MataKuliahController extends Controller
{
    private $mataKuliahModel;
    public function __construct()
    {
        $this->mataKuliahModel = new MataKuliah();
    }

    public function index(Request $request)
    {
        $getAllMataKuliah = $this->mataKuliahModel->with('prodi'); // select * from MataKuliah;

        $prodi = $request->prodi_id;

        if($request->filled('prodi_id')){
            $getAllMataKuliah = $getAllMataKuliah->whereHas('prodi',function($query) use ($prodi){
                return $query->where('id',$prodi);
            });
        }

        $getAllMataKuliah = $getAllMataKuliah->get(); // select * from MataKuliah;
        return response()->json($getAllMataKuliah);
    }

    public function store(Request $request)
    {
        $createNewMataKuliah = $this->mataKuliahModel->create([
            'prodi_id' => $request->prodi_id,
            'nama_mk' => $request->nama_mk,
            'kode_mk' => $request->kode_mk,
            'sks_mk' => $request->sks_mk,
        ]);
        return response()->json($createNewMataKuliah);
    }

    public function show($id)
    {
        $findMataKuliah = $this->mataKuliahModel->with('prodi')->find($id);
        return response()->json($findMataKuliah);
    }

    public function update($id, Request $request)
    {
        $findMataKuliah = $this->mataKuliahModel->find($id);
        $findMataKuliah->update([
            'prodi_id' => $request->prodi_id,
            'nama_mk' => $request->nama_mk,
            'kode_mk' => $request->kode_mk,
            'sks_mk' => $request->sks_mk,
        ]);
        return response()->json($findMataKuliah);
    }

    public function destroy($id)
    {
        $findMataKuliah = $this->mataKuliahModel->find($id);
        $findMataKuliah->delete();
        return response()->json($findMataKuliah);
    }

    public function kaprodimk (Request $request)
    {

        $userProdi = auth()->user()->prodi_id;

        $kaprodiMk = MataKuliah::with(['prodi'])
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id',$userProdi)
        ->selectRaw('mata_kuliah.id , mata_kuliah.nama_mk')
        ->groupBy('id','nama_mk')
        ->get();

        return response()->json(['data' => $kaprodiMk]);

    }
}
