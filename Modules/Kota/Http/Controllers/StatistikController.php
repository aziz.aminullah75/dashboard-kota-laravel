<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Kota\Entities\Statistik;

class StatistikController extends Controller
{
    private $statistikModel;
    public function __construct()
    {
        $this->statistikModel = new Statistik();
    }

    public function index()
    {
        $getAllStatistik = $this->statistikModel->with('laporan.mata_kuliah')->get(); // select * from Statistik;
        return response()->json($getAllStatistik);
    }

    public function store(Request $request)
    {
        $createNewStatistik = $this->statistikModel->create([
            'laporan_id' => $request->laporan_id,
            'total_kuota_dibuka' => $request->total_kuota_dibuka,
            'total_kuota_terisi' => $request->total_kuota_terisi,
        ]);
        return response()->json($createNewStatistik);
    }

    public function show($id)
    {
        $findStatistik = $this->statistikModel->with('laporan.mata_kuliah')->find($id);
        return response()->json($findStatistik);
    }

    public function update($id, Request $request)
    {
        $findStatistik = $this->statistikModel->find($id);
        $findStatistik->update([
            'laporan_id' => $request->laporan_id,
            'total_kuota_dibuka' => $request->total_kuota_dibuka,
            'total_kuota_terisi' => $request->total_kuota_terisi,
        ]);
        return response()->json($findStatistik);
    }

    public function destroy($id)
    {
        $findStatistik = $this->statistikModel->find($id);
        $findStatistik->delete();
        return response()->json($findStatistik);
    }
}
