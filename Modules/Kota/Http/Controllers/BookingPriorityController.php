<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Kota\Entities\BookingPriority;
// use Modules\Auth\Entities\User;

class BookingPriorityController extends Controller
{
    private $bookingPriorityModel;
    public function __construct()
    {
        $this->bookingPriorityModel = new BookingPriority();
    }

    public function index(Request $request)
    {
        $getPriorityBooking = $this->bookingPriorityModel->with('users.prodi', 'periode','mata_kuliah.prodi');

        $periode = $request->periode_id;
        $prodi = $request->prodi_id;

        if($request->filled('periode_id')){
            $getPriorityBooking = $getPriorityBooking->where('periode_id',$periode);
        }
        if($request->filled('prodi_id')){
            $getPriorityBooking = $getPriorityBooking->whereHas('users.prodi',function($query) use ($prodi){
                return $query->where('id',$prodi);
            });
        }



        $getPriorityBooking = $getPriorityBooking->get(); // select * from MataKuliah;
        return response()->json($getPriorityBooking);
    }

    // public function index()
    // {
    //     $getAllBookingPriority = $this->bookingPriorityModel->with('periode','mata_kuliah.prodi')->get(); // select * from MataKuliah;
    //     return response()->json($getAllBookingPriority);
    // }

    public function store(Request $request)
    {
        // $userId = auth()->id();
        $createNewBookingPriority = $this->bookingPriorityModel->create([
            'users_id' => auth()->id(),
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'peserta' => $request->peserta,
        ]);
        return response()->json($createNewBookingPriority);
    }

    public function show($id)
    {
        $findBookingPriority = $this->bookingPriorityModel->with('users.prodi', 'periode','mata_kuliah.prodi')->find($id);
        return response()->json($findBookingPriority);
    }

    public function update($id, Request $request)
    {
        $userId = auth()->user()->id;
        $findBookingPriority = $this->bookingPriorityModel->find($id);
        $findBookingPriority->update([
            'users_id' => $userId,
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'peserta' => $request->peserta,
        ]);
        return response()->json($findBookingPriority);
    }

    public function destroy($id)
    {
        $findBookingPriority = $this->bookingPriorityModel->find($id);
        $findBookingPriority->delete();
        return response()->json($findBookingPriority);
    }

    // public function bookedList (Request $request)
    // {
    //     $periodeId = $request->periode_id;
    //     $prodiId = $request->prodi_id;

    //     $jumlah = DB::table('booking')
    //     ->join('mata_kuliah','booking_priority.mata_kuliah_id','=','mata_kuliah.id')
    //     ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
    //     ->where('periode_id', $periodeId)->where('prodi_id', $prodiId)
    //     // ->selectRaw('sum(kuota_dibuka) as total_kuota_dibuka,sum(kuota_terisi) as total_kuota_terisi, (sum(kuota_dibuka) - sum(kuota_terisi)) as total_kouta_tersedia, sum(cadangan) as total_cadangan')
    //     ->first();

    //     return response()->json($jumlah);

    // }

    // public function booked (Request $request)
    // {

    //     $userProdi = auth()->user()->prodi_id;

    //     $booking = BookingPriority::with(['prodi','mata_kuliah','periode'])
    //     ->join('mata_kuliah','booking_priority.mata_kuliah_id','=','mata_kuliah.id')
    //     // ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
    //     ->where('prodi_id',$userProdi)->get();

    //     return response()->json(['data' => $booking]);

    // }

    public function booked (Request $request)
    {

        $userId = auth()->user()->id;
        $periodeId = $request->periode_id;


        $booking = BookingPriority::with(['mata_kuliah','periode'])

        ->join('periode','booking_priority.periode_id','=','periode.id')
        ->join('mata_kuliah','booking_priority.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->join('users','booking_priority.users_id','=','users.id')
        ->where('users_id',$userId)->where('periode_id', $periodeId)
        ->selectRaw('mata_kuliah.nama_mk, mata_kuliah.kode_mk, booking_priority.id, booking_priority.peserta, periode.tahun, periode.semester, prodi.singkatan_prodi')
        ->groupBy('id','peserta', 'nama_mk','kode_mk','tahun','semester','singkatan_prodi')
        ->get();

        return response()->json(['data' => $booking]);

    }
}
