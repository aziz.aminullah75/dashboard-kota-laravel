<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Kota\Entities\Booking;

class BookingController extends Controller
{
    private $bookingModel;
    public function __construct()
    {
        $this->bookingModel = new Booking();
    }

    public function index(Request $request)
    {
        $getBooking = $this->bookingModel->with('periode','mata_kuliah.prodi');

        $periode = $request->periode_id;
        $prodi = $request->prodi_id;

        if($request->filled('periode_id')){
            $getBooking = $getBooking->where('periode_id',$periode);
        }
        if($request->filled('prodi_id')){
            $getBooking = $getBooking->whereHas('mata_kuliah.prodi',function($query) use ($prodi){
                return $query->where('id',$prodi);
            });
        }



        $getBooking = $getBooking->get(); // select * from MataKuliah;
        return response()->json($getBooking);
    }

    public function store(Request $request)
    {
        $createNewBooking = $this->bookingModel->create([
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'peserta' => $request->peserta,
        ]);
        return response()->json($createNewBooking);
    }

    public function show($id)
    {
        $findBooking = $this->bookingModel->with('periode','mata_kuliah.prodi')->find($id);
        return response()->json($findBooking);
    }

    public function update($id, Request $request)
    {
        $findBooking = $this->bookingModel->find($id);
        $findBooking->update([
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'peserta' => $request->peserta,
        ]);
        return response()->json($findBooking);
    }

    public function destroy($id)
    {
        $findBooking = $this->bookingModel->find($id);
        $findBooking->delete();
        return response()->json($findBooking);
    }

    public function bookedList (Request $request)
    {
        $periodeId = $request->periode_id;
        $prodiId = $request->prodi_id;

        $jumlah = DB::table('booking')
        ->join('mata_kuliah','booking.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('periode_id', $periodeId)->where('prodi_id', $prodiId)
        // ->selectRaw('sum(kuota_dibuka) as total_kuota_dibuka,sum(kuota_terisi) as total_kuota_terisi, (sum(kuota_dibuka) - sum(kuota_terisi)) as total_kouta_tersedia, sum(cadangan) as total_cadangan')
        ->first();

        return response()->json($jumlah);

    }

    public function booked (Request $request)
    {

        $userProdi = auth()->user()->prodi_id;

        $booking = Booking::with(['mata_kuliah','periode'])
        ->join('mata_kuliah','booking.mata_kuliah_id','=','mata_kuliah.id')
        // ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id',$userProdi)->get();

        return response()->json(['data' => $booking]);

    }
}
