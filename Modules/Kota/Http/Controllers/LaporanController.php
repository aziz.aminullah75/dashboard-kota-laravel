<?php

namespace Modules\Kota\Http\Controllers;

// use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Kota\Entities\Laporan;
use Symfony\Component\HttpKernel\HttpCache\ResponseCacheStrategy;

class LaporanController extends Controller
{
    private $laporanModel;
    public function __construct()
    {
        $this->laporanModel = new Laporan();
    }

    public function index(Request $request)
    {
        $getLaporan = $this->laporanModel->with('jadwal','periode','mata_kuliah.prodi','dosen');

        $periode = $request->periode_id;
        $prodi = $request->prodi_id;

        if($request->filled('periode_id')){
            $getLaporan = $getLaporan->where('periode_id',$periode);
        }
        if($request->filled('prodi_id')){
            $getLaporan = $getLaporan->whereHas('mata_kuliah.prodi',function($query) use ($prodi){
                return $query->where('id',$prodi);
            });
        }



        $getLaporan = $getLaporan->get(); // select * from MataKuliah;
        return response()->json($getLaporan);
    }

    public function store(Request $request)
    {
        $createNewLaporan = $this->laporanModel->create([
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'jadwal_id' => $request->jadwal_id,
            'dosen_id' => $request->dosen_id,
            'ruangan' => $request->ruangan,
            'kuota_dibuka' => $request->kuota_dibuka,
            'kuota_terisi' => $request->kuota_terisi ?? 0,
            'cadangan' => $request->cadangan ?? 0,
        ]);
        return response()->json($createNewLaporan);
    }

    public function show($id)
    {
        $findLaporan = $this->laporanModel->with('jadwal','periode','mata_kuliah.prodi','dosen')->find($id);
        return response()->json($findLaporan);
    }

    public function update($id, Request $request)
    {
        $findLaporan = $this->laporanModel->find($id);
        $findLaporan->update([
            'periode_id' => $request->periode_id,
            'mata_kuliah_id' => $request->mata_kuliah_id,
            'jadwal_id' => $request->jadwal_id,
            'dosen_id' => $request->dosen_id,
            'ruangan' => $request->ruangan,
            'kuota_dibuka' => $request->kuota_dibuka,
            'kuota_terisi' => $request->kuota_terisi,
            'cadangan' => $request->cadangan,
        ]);
        return response()->json($findLaporan);
    }

    public function destroy($id)
    {
        $findLaporan = $this->laporanModel->find($id);
        $findLaporan->delete();
        return response()->json($findLaporan);
    }

    public function report (Request $request)
    {
        $periodeId = $request->periode_id;
        $prodiId = $request->prodi_id;

        $jumlah = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('periode_id', $periodeId)->where('prodi_id', $prodiId)
        ->selectRaw('sum(kuota_dibuka) as total_kuota_dibuka,sum(kuota_terisi) as total_kuota_terisi, (sum(kuota_dibuka) - sum(kuota_terisi)) as total_kouta_tersedia, sum(cadangan) as total_cadangan')->first();

        return response()->json($jumlah);

    }

    public function kmeans ()
    // public function report (Request $request)
    {
        // $prodiId = $request->prodi_id;

        $mataKuliah = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        // ->where('prodi_id', $prodiId)
        ->selectRaw('mata_kuliah.nama_mk,mata_kuliah.kode_mk,sum(kuota_dibuka) as total_dibuka,sum(kuota_terisi) as total_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','kode_mk','singkatan_prodi')
        ->get();

        return response()->json(['data' => $mataKuliah]);
    }

    public function graph ()
    {

        $data1 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 1)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data2 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 2)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data3 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 3)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data4 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 4)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data5 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 5)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data6 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 6)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data7 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 7)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data8 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 8)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data9 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 9)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $data10 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi_id', '=', 10)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as graph_dibuka,sum(kuota_terisi) as graph_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk','singkatan_prodi')
        ->orderBy('graph_terisi', 'desc')
        ->first();

        $dataGraph = [$data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10];
        // $dataGraph = [$data6, $data7];

        return response()->json(['data' => $dataGraph]);

    }

    public function urban (Request $request)
    {

        $urbanId = $request->urban_id;

        $urban1 = DB::table('laporan')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('prodi.urban_id', '=', $urbanId)
        ->selectRaw('mata_kuliah.nama_mk,sum(kuota_dibuka) as kuota_urban_dibuka,sum(kuota_terisi) as kuota_urban_terisi, prodi.singkatan_prodi')
        ->groupBy('mata_kuliah_id','nama_mk', 'singkatan_prodi')
        ->orderBy('kuota_urban_terisi', 'desc')
        ->limit(5)
        ->get();

        // $dataUrban = [$urban1;

        return response()->json(['data' => $urban1]);

    }

    public function kaprodi (Request $request)
    {

        $userProdi = auth()->user()->prodi_id;
        $periodeId = $request->periode_id;


        $laporan = Laporan::with(['mata_kuliah','periode','jadwal','dosen'])

        ->join('periode','laporan.periode_id','=','periode.id')
        ->join('jadwal','laporan.jadwal_id','=','jadwal.id')
        ->join('dosen','laporan.dosen_id','=','dosen.id')
        ->join('mata_kuliah','laporan.mata_kuliah_id','=','mata_kuliah.id')
        ->join('prodi','prodi.id','=','mata_kuliah.prodi_id')
        ->where('mata_kuliah.prodi_id',$userProdi)->where('periode_id',$periodeId)
        ->selectRaw('mata_kuliah.nama_mk, mata_kuliah.kode_mk, mata_kuliah.sks_mk, laporan.id, periode.tahun, periode.semester, jadwal.hari, jadwal.jam, laporan.ruangan, laporan.kuota_dibuka, dosen.nama')
        ->groupBy('id','nama_mk','kode_mk','sks_mk','tahun','semester','hari','jam','ruangan','kuota_dibuka','nama')
        ->get();

        return response()->json(['data' => $laporan]);

    }
}
