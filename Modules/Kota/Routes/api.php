<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/kota', function (Request $request) {
    return $request->user();
});

Route::prefix('/kota')->group(function () {
    Route::prefix('periode')->group(function () {
        Route::get('/', 'PeriodeController@index');
        Route::post('/create', 'PeriodeController@store');
        Route::get('/get/{id}', 'PeriodeController@show');
        Route::post('/update/{id}', 'PeriodeController@update');
        Route::delete('/delete/{id}', 'PeriodeController@destroy');
    });
    Route::prefix('jadwal')->group(function () {
        Route::get('/', 'JadwalController@index');
        Route::post('/create', 'JadwalController@store');
        Route::get('/get/{id}', 'JadwalController@show');
        Route::post('/update/{id}', 'JadwalController@update');
    });
    Route::prefix('mata-kuliah')->group(function () {
        Route::get('/', 'MataKuliahController@index');
        Route::post('/create', 'MataKuliahController@store');
        Route::get('/get/{id}', 'MataKuliahController@show');
        Route::post('/update/{id}', 'MataKuliahController@update');
        Route::delete('/delete/{id}', 'MataKuliahController@destroy');
        // Route::post('/filter', 'LaporanController@filter');
        Route::post('/kaprodimk', 'MataKuliahController@kaprodimk')->middleware('auth:sanctum');
    });
    Route::prefix('dosen')->group(function () {
        Route::get('/', 'DosenController@index');
        Route::post('/create', 'DosenController@store');
        Route::get('/get/{id}', 'DosenController@show');
        Route::post('/update/{id}', 'DosenController@update');
        Route::delete('/delete/{id}', 'DosenController@destroy');
        Route::post('/kaprodi-dosen', 'DosenController@kaprodiDosen')->middleware('auth:sanctum');
    });
    Route::prefix('laporan')->group(function () {
        Route::get('/', 'LaporanController@index');
        Route::post('/create', 'LaporanController@store')->middleware('auth:sanctum');
        Route::get('/get/{id}', 'LaporanController@show')->middleware('auth:sanctum');
        Route::post('/update/{id}', 'LaporanController@update')->middleware('auth:sanctum');
        Route::delete('/delete/{id}', 'LaporanController@destroy')->middleware('auth:sanctum');
        Route::post('/report', 'LaporanController@report');
        Route::post('/kmeans', 'LaporanController@kmeans');
        Route::post('/graph', 'LaporanController@graph');
        Route::post('/urban', 'LaporanController@urban');
        Route::post('/kaprodi', 'LaporanController@kaprodi')->middleware('auth:sanctum');
    });
    Route::prefix('booking-priority')->group(function () {
        Route::get('/', 'BookingPriorityController@index');
        Route::post('/create', 'BookingPriorityController@store')->middleware('auth:sanctum');
        Route::get('/get/{id}', 'BookingPriorityController@show')->middleware('auth:sanctum');
        Route::post('/update/{id}', 'BookingPriorityController@update')->middleware('auth:sanctum');
        Route::delete('/delete/{id}', 'BookingPriorityController@destroy')->middleware('auth:sanctum');
        Route::post('/booked-list', 'BookingPriorityController@bookedList');
        Route::post('/booked', 'BookingPriorityController@booked')->middleware('auth:sanctum');
    });
    Route::prefix('statistik')->group(function () {
        Route::get('/', 'StatistikController@index');
        Route::post('/create', 'StatistikController@store');
        Route::get('/get/{id}', 'StatistikController@show');
        Route::post('/update/{id}', 'StatistikController@update');
    });
});
