<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Kota\Entities\Periode;
use Modules\Kota\Entities\MataKuliah;
use Modules\Kota\Entities\Jadwal;
use Modules\Kota\Entities\Dosen;

class Laporan extends Model
{
    protected $fillable = ['periode_id','mata_kuliah_id','jadwal_id','dosen_id','ruangan','dosen_pengampu','kuota_dibuka','kuota_terisi','cadangan'];

    protected $table = 'laporan';

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }
    public function mata_kuliah()
    {
        return $this->belongsTo(MataKuliah::class, 'mata_kuliah_id');
    }
    public function jadwal()
    {
        return $this->belongsTo(Jadwal::class, 'jadwal_id');
    }
    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'dosen_id');
    }
}
