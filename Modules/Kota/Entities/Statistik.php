<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Kota\Entities\Laporan;

class Statistik extends Model
{
    protected $fillable = ['laporan_id','total_kuota_dibuka','total_kuota_terisi'];

    protected $table = 'statistik';

    public function laporan()
    {
        return $this->belongsTo(Laporan::class, 'laporan_id');
    }
}
