<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['hari','jam'];

    protected $table = 'jadwal';
}
