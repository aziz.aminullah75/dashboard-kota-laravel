<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Kota\Entities\Periode;
use Modules\Kota\Entities\MataKuliah;

class BookingPriority extends Model
{
    protected $fillable = ['users_id','periode_id','mata_kuliah_id','peserta'];
    // protected $fillable = ['periode_id','mata_kuliah_id','peserta'];

    protected $table = 'booking_priority';

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id');
    }
    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }
    public function mata_kuliah()
    {
        return $this->belongsTo(MataKuliah::class, 'mata_kuliah_id');
    }
}
