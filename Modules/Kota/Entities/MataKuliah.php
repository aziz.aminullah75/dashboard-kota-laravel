<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Prodi;
class MataKuliah extends Model
{
    protected $fillable = ['prodi_id','nama_mk','kode_mk','sks_mk'];

    protected $table = 'mata_kuliah';

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'prodi_id');
    }
}
