<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Kota\Entities\Periode;
use Modules\Kota\Entities\MataKuliah;

class Booking extends Model
{
    protected $fillable = ['periode_id','mata_kuliah_id','peserta'];

    protected $table = 'booking';

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }
    public function mata_kuliah()
    {
        return $this->belongsTo(MataKuliah::class, 'mata_kuliah_id');
    }
}
