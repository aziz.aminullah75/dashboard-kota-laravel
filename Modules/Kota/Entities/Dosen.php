<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Prodi;

class Dosen extends Model
{
    protected $fillable = ['prodi_id','nama','nik','jk','email'];

    protected $table = 'dosen';

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'prodi_id');
    }
}
