<?php

namespace Modules\Kota\Entities;

use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    protected $fillable = ['tahun','semester'];

    protected $table = 'periode';
}
